package ru.edu.ProjectShop.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.entity.UserRole;
import ru.edu.ProjectShop.repository.AccountRepository;
import ru.edu.ProjectShop.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


@ExtendWith(MockitoExtension.class)
public class AdminServiceImplTests {

    @InjectMocks
    private AdminServiceImpl adminService;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private ProductRepository productRepository;


    private final Account admin = new Account(1L, "admin", "admin@mail.ru", "Admin", "Admin", UserRole.ADMIN);
    private final Account user = new Account(2L, "user", "user@mail.ru", "User", "User", UserRole.USER);
    private final List<Account> accounts = Arrays.asList(admin, user);
    private final Product product1 = new Product(1L, "game1", "imgUrl1", new BigDecimal(1200), 2, "good game");
    private final Product product2 = new Product(2L, "game2", "imgUrl2", new BigDecimal(1000), 3, "bad game");
    private final List<Product> products = Arrays.asList(product1, product2);

    /**
     * Проверка корректной работы метода AdminServiceImpl#allAccounts()
     */
    @Test
    public void allAccounts_Test() {
        Mockito.when(accountRepository.findAll()).thenReturn(accounts);
        Assertions.assertEquals(accounts, adminService.allAccounts());
    }

    /**
     * Проверка корректной работы метода AdminServiceImpl#deleteAccountById(Long id)
     */
    @Test
    public void deleteAccountById_Test() {
        adminService.deleteAccountById(2L);
        Mockito.verify(accountRepository, Mockito.times(1)).deleteById(2L);
    }

    /**
     * Проверка корректной работы метода AdminServiceImpl#changeRoleAccountById(Long id)
     */
    @Test
    public void changeRoleAccountById_Test() {
        Mockito.when(accountRepository.getReferenceById(1L)).thenReturn(admin);
        adminService.changeRoleAccountById(1L);
        Assertions.assertEquals(UserRole.USER, admin.getRole());
    }

    /**
     * Проверка корректной работы метода AdminServiceImpl#allProducts()
     */
    @Test
    public void allProducts_Test() {
        Mockito.when(productRepository.findAll()).thenReturn(products);
        Assertions.assertEquals(products, adminService.allProducts());
    }

    /**
     * Проверка корректной работы метода AdminServiceImpl#createProduct(Product product)
     */
    @Test
    public void createProduct_Test() {
        adminService.createProduct(product1);
        Mockito.verify(productRepository, Mockito.times(1)).save(product1);
    }

    /**
     * Проверка корректной работы метода AdminServiceImpl#deleteProductById(Long id)
     */
    @Test
    public void deleteProductById_Test() {
        adminService.deleteProductById(1L);
        Mockito.verify(productRepository, Mockito.times(1)).deleteById(1L);
    }

    /**
     * Проверка корректной работы метода AdminServiceImpl#getProductById(Long id)
     */
    @Test
    public void getProductById_Test() {
        Mockito.when(productRepository.getReferenceById(2L)).thenReturn(product2);
        Assertions.assertEquals(product2, adminService.getProductById(2L));
    }
}
