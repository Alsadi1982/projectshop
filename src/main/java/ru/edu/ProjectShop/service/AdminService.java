package ru.edu.ProjectShop.service;

import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.entity.UserRole;

import java.util.List;

public interface AdminService {

    List<Account> allAccounts();

    void deleteAccountById(Long id);

    void changeRoleAccountById(Long id);

    List<Product> allProducts();

    void createProduct(Product product);

    void deleteProductById(Long id);

    Product getProductById(Long id);
    UserRole getRole(Account account);

}
