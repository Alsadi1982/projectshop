package ru.edu.ProjectShop.service;

import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.entity.UserRole;

import java.math.BigDecimal;
import java.util.Map;

public interface ShoppingCartService {

    void addProduct(Product product);
    Map<Product, Integer> getAllProductsFromShoppingCart();
    void removeProductsFromShoppingCart(Product product);
    BigDecimal getTotalCount();
    UserRole getRole(Account account);
    Product getProductById(Long id);
    void removeAllProductsFromShoppingCart();

}
