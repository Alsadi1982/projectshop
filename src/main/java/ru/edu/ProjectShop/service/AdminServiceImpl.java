package ru.edu.ProjectShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.entity.UserRole;
import ru.edu.ProjectShop.repository.AccountRepository;
import ru.edu.ProjectShop.repository.ProductRepository;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {

    private final AccountRepository accountRepository;
    private final ProductRepository productRepository;

    @Autowired
    public AdminServiceImpl(AccountRepository accountRepository, ProductRepository productRepository) {
        this.accountRepository = accountRepository;
        this.productRepository = productRepository;
    }

    /**
     * Метод AdminServiceImpl#allAccounts() выводит список всех пользователей сохраненных в БД
     * @return
     */
    @Override
    public List<Account> allAccounts() {
        return accountRepository.findAll();
    }

    /**
     * Метод AdminServiceImpl#deleteAccountById(Long id) удаляет пользователя с данным Id из БД
     * @param id пользователя
     */
    @Override
    public void deleteAccountById(Long id) {
        accountRepository.deleteById(id);
    }

    /**
     * Метод AdminServiceImpl#changeRoleAccountById(Long id) меняет роль пользователя с данным Id БД
     * @param id пользователя
     */
    @Override
    public void changeRoleAccountById(Long id) {
        Account account = accountRepository.getReferenceById(id);
        if (account.getRole().equals(UserRole.USER)) {
            account.setRole(UserRole.ADMIN);
        } else {
            account.setRole(UserRole.USER);
        }
        accountRepository.save(account);
    }

    /**
     * Метод AdminServiceImpl#allProducts() выводит список всех товаров сохраненных в БД
     * @return
     */
    @Override
    public List<Product> allProducts() {
        return productRepository.findAll();
    }

    /**
     * Метод AdminServiceImpl#createProduct(Product product) Создает новый товар и сохраняет его в БД
     * @param product
     */
    @Override
    public void createProduct(Product product) {
        productRepository.save(product);
    }

    /**
     * Метод AdminServiceImpl#deleteProductById(Long id) удаляет товар с данным Id из БД
     * @param id товара
     */
    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }

    /**
     * Метод AdminServiceImpl#getProductById(Long id) возвращает товар заданного Id из БД
     * @param id товара
     * @return товар с данным Id
     */
    @Override
    public Product getProductById(Long id) {
        return productRepository.getReferenceById(id);
    }

    /**
     * Метод AdminServiceImpl#getRole(Account account) возвращает роль пользователя
     * @param account
     * @return
     */
    @Override
    public UserRole getRole(Account account) {
        return accountRepository.findAccountByEmail(account.getEmail()).getRole();
    }
}
