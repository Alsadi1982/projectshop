package ru.edu.ProjectShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.repository.ProductRepository;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * ProductServiceImpl#allProducts() возвращает список товаров из БД
     * @return
     */
    @Override
    public List<Product> allProducts() {
        return productRepository.findAll();
    }


    /**
     * Метод ProductServiceImpl#getProductById(Long id) возвращает товар заданного Id из БД
     * @param id товара
     * @return товар с данным Id
     */
    @Override
    public Product getProductById(Long id) {
        return productRepository.getReferenceById(id);
    }

    @Override
    public void createProduct(Product product) {
        productRepository.save(product);
    }
}
