package ru.edu.ProjectShop.service;

import ru.edu.ProjectShop.entity.Account;

public interface RegistrationService {

    void saveAccount(Account account);

    Account getAccountByEmail(String email);
}
