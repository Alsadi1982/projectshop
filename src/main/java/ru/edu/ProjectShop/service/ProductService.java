package ru.edu.ProjectShop.service;

import ru.edu.ProjectShop.entity.Product;

import java.util.List;

public interface ProductService {
    
    List<Product> allProducts();

    Product getProductById(Long id);
    void createProduct(Product product);
}
