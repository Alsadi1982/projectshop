package ru.edu.ProjectShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.entity.UserRole;
import ru.edu.ProjectShop.repository.AccountRepository;
import ru.edu.ProjectShop.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private final ProductRepository productRepository;
    private final AccountRepository accountRepository;

    private Map<Product, Integer> products = new HashMap<>();

    @Autowired
    public ShoppingCartServiceImpl(ProductRepository productRepository, AccountRepository accountRepository) {
        this.productRepository = productRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * Метод ShoppingCartServiceImpl#addProduct(Product product) добавляет товар в корзину
     * @param product
     */
    @Override
    public void addProduct(Product product) {

        if (products.containsKey(product)) {
            products.replace(product, products.get(product) + 1);
        } else {
            products.put(product, 1);
        }
    }

    /**
     * Метод ShoppingCartServiceImpl#getAllProductsFromShoppingCart() возвращает корзину с продуктами
     * @return
     */
    @Override
    public Map<Product, Integer> getAllProductsFromShoppingCart() {
        return Collections.unmodifiableMap(products);
    }

    /**
     * Метод ShoppingCartServiceImpl#removeProductsFromShoppingCart(Product product) удаляет продукт из корзины
     * @param product
     */
    @Override
    public void removeProductsFromShoppingCart(Product product) {

        if (products.containsKey(product)){
            if (products.get(product) > 1) {
                products.replace(product, products.get(product) - 1);
            } else {
                products.remove(product);
            }
        }
    }

    /**
     * Метод ShoppingCartServiceImpl#getTotalCount() вычисляет общую сумму заказа
     * @return возвращает общую сумму заказа
     */
    @Override
    public BigDecimal getTotalCount() {
        return products.entrySet()
                .stream()
                .map(entry -> entry.getKey().getPrice().multiply(BigDecimal.valueOf(entry.getValue())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    @Override
    public Product getProductById(Long id) {
        for (Map.Entry<Product, Integer> elem: products.entrySet()) {
            if (elem.getKey().getId().equals(id)) {
                return elem.getKey();
            }
        }
        return null;
    }

    @Override
    public void removeAllProductsFromShoppingCart() {
        products.clear();
    }

    /**
     * Метод ShoppingCartServiceImpl#getRole(Account account) возвращает роль пользователя
     * @param account
     * @return
     */
    @Override
    public UserRole getRole(Account account) {
        return accountRepository.findAccountByEmail(account.getEmail()).getRole();
    }
}
