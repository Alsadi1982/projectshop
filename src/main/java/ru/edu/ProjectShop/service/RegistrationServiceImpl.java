package ru.edu.ProjectShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.repository.AccountRepository;

@Service
public class RegistrationServiceImpl implements RegistrationService {


    private final AccountRepository accountRepository;

    @Autowired
    public RegistrationServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /**
     * Метод RegistrationServiceImpl#saveAccount(Account account) регистрирует пользователя в магазине
     * @param account
     */
    @Override
    public void saveAccount(Account account) {

        accountRepository.save(account);
    }

    /**
     * Метод RegistrationServiceImpl#getAccountByEmail(String email) ищет пользователя по email
     * @param email
     * @return
     */
    @Override
    public Account getAccountByEmail(String email) {
        return accountRepository.findAccountByEmail(email);
    }
}
