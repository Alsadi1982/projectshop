package ru.edu.ProjectShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.edu.ProjectShop.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {


}
