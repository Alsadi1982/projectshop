package ru.edu.ProjectShop.validator;

import org.passay.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;
@Component
public class SimplePasswordValidator {

    public boolean isValid(String password) {
        PasswordValidator passwordValidator = new PasswordValidator(Arrays.asList(
                new LengthRule(8, 10),
                new WhitespaceRule()));
        RuleResult result = passwordValidator.validate(new PasswordData(password));

        return result.isValid();
    }


}
