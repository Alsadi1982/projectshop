package ru.edu.ProjectShop.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;


@Component
public class SimpleEmailValidator implements EmailValidator {
    @Value("${emailvalidator.regexPattern}")
    private String regexPattern;

    @Override
    public boolean patternMatches(String emailAddress) {
        return Pattern.compile(this.regexPattern)
                .matcher(emailAddress)
                .matches();
    }
}
