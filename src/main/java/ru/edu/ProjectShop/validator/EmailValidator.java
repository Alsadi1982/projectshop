package ru.edu.ProjectShop.validator;

public interface EmailValidator {

    boolean patternMatches(String emailAddress);
}
