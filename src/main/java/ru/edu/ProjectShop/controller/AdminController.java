package ru.edu.ProjectShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.entity.UserRole;
import ru.edu.ProjectShop.security.AwesomeShopAccountPrincipal;
import ru.edu.ProjectShop.service.AdminService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    private final AdminService adminService;

    @Autowired
    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    /**
     * Страница основного меню администратора
     * @return
     */
    @GetMapping()
    public String index(Model model, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        return "admin/index";
    }

    /**
     * Список всех пользователей
     * @param model
     * @return
     */
    @GetMapping("/user")
    public String userList(Model model, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        model.addAttribute("users", adminService.allAccounts());
        return "admin/user_list";
    }

    /**
     * Удаление аккаунта пользователя
     * @param model
     * @param id
     * @return
     */
    @PostMapping("/user/delete/{id}")
    public String deleteAccount(Model model, @PathVariable("id") Long id, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {

        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        try {
            adminService.deleteAccountById(id);
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
        }
        model.addAttribute("users", adminService.allAccounts());
        return "admin/user_list";
    }

    /**
     * Меняет роль у пользователя
     * @param model
     * @param id
     * @return
     */
    @PostMapping("/user/change_role/{id}")
    public String changeAccountRole(Model model, @PathVariable("id") Long id, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        try {
            adminService.changeRoleAccountById(id);
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
        }
        model.addAttribute("users", adminService.allAccounts());
        return "admin/user_list";
    }



    /**
     * Список всех товаров
     * @param model
     * @return
     */
    @GetMapping("/product")
    public String productList(Model model, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {

        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        model.addAttribute("products", adminService.allProducts());
        return "admin/product_list";
    }

    /**
     * Создание нового товара - сохранение товара
     * @param model
     * @param product
     * @return
     */
    @PostMapping("/product/new")
    public String createProduct(Model model, @ModelAttribute Product product) {

        adminService.createProduct(product);
        return "redirect:/admin/product";

    }

    /**
     * Создание нового товара - отрисовка страницы
     * @param model
     * @return
     */
    @GetMapping("/product/new")
    public String productNew(Model model, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        model.addAttribute("product", new Product());
        return "admin/product_new";

    }

    /**
     * Удаление выбранного товара
     * @param model
     * @param id
     * @return
     */
    @PostMapping("/product/delete/{id}")
    public String deleteProduct(Model model, @PathVariable("id") Long id, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        try {
            adminService.deleteProductById(id);
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
        }
        model.addAttribute("products", adminService.allProducts());
        return "admin/product_list";
    }

    /**
     * Редактирование товара - отрисовка страницы
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/product/{id}")
    public String productEdit(Model model, @PathVariable("id") Long id, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        model.addAttribute("product", adminService.getProductById(id));
        return "admin/product_edit";
    }

    /**
     * Редактирование товара - сохранение изменений
     * @param product
     * @return
     */
    @PostMapping("/product/{id}")
    public String productEditPost(@ModelAttribute("product") Product product) {
        adminService.createProduct(product);
        return "redirect:/admin/product";
    }

    /**
     * Метод AdminController#isRole(Account account) проверяет роль пользователя
     * @param account
     * @return
     */
    private boolean isRole(Account account) {
        return adminService.getRole(account).equals(UserRole.ADMIN);
    }


}
