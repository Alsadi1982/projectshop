package ru.edu.ProjectShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.entity.UserRole;
import ru.edu.ProjectShop.security.AwesomeShopAccountPrincipal;
import ru.edu.ProjectShop.service.ProductService;
import ru.edu.ProjectShop.service.ShoppingCartService;

import java.util.List;
import java.util.Map;


@Controller
@RequestMapping(value = "/user")
public class UserController {

    private final ProductService productService;
    private final ShoppingCartService shoppingCartService;

    private boolean isBoth = false;

    @Autowired
    public UserController(ProductService productService, ShoppingCartService shoppingCartService) {
        this.productService = productService;
        this.shoppingCartService = shoppingCartService;
    }

    /**
     * Корзина покупателя - отрисовка страницы
     * @param model
     * @return
     */
    @GetMapping
    public String getShoppingCart(Model model, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        Map<Product, Integer> products = shoppingCartService.getAllProductsFromShoppingCart();
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        model.addAttribute("products", products);
        model.addAttribute("totalCount", shoppingCartService.getTotalCount());
        model.addAttribute("both", isBoth);
        return "user/user_shopping_cart";
    }

    /**
     * Выбор товара покупателем - отрисовка страницы
     * @param model
     * @return
     */
    @GetMapping("/product")
    public String userProductList(Model model, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        final List<Product> products = productService.allProducts();
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        model.addAttribute("products", products);
        isBoth = false;
        return "user/user_product_list";
    }

    /**
     * Страница информации о конкретном товаре - отрисовка страницы
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/product/{id}")
    public String getProductInfo(Model model, @PathVariable("id") Long id, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        model.addAttribute("product", productService.getProductById(id));
        return "user/user_product_info";
    }

    /**
     * Добавление товара в корзину
     * @param model
     * @param id
     * @return
     */
    @PostMapping("/product/{id}")
    public String addProductIntoShoppingCart(Model model, @PathVariable("id") Long id) {
        Product product = productService.getProductById(id);
        product.setAmount(product.getAmount() - 1);
        productService.createProduct(product);
        shoppingCartService.addProduct(product);
        return "redirect:/user/product";
    }

    /**
     * Удаление товара из корзины
     * @param model
     * @param id
     * @param principal
     * @return
     */
    @GetMapping("/delete/{id}")
    public String deleteProductFromShoppingCart(Model model, @PathVariable("id") Long id, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        if (principal != null) {
            if (!isRole(principal.getAccount())) {
                return "error/access_denied";
            } else {
                model.addAttribute("current", principal.getAccount());
            }
        }
        try {
            Product product = shoppingCartService.getProductById(id);
            Product product2 = productService.getProductById(id);
            product2.setAmount(product2.getAmount() + shoppingCartService.getAllProductsFromShoppingCart().get(product));
            productService.createProduct(product2);
            shoppingCartService.removeProductsFromShoppingCart(product);
        } catch (Exception e) {
            model.addAttribute("message", e.getMessage());
        }
        return "redirect:/user";
    }


    /**
     * Покупатель делает покупку, все товары из корзины удаляются
     * @return
     */
    @PostMapping("/buying")
    public String byAllProduct() {
        shoppingCartService.removeAllProductsFromShoppingCart();
        isBoth = true;

        return "redirect:/user";
    }

    /**
     * Метод UserController#isRole() проверяет роль пользователя
     * @param account
     * @return
     */
    private boolean isRole(Account account) {
        return shoppingCartService.getRole(account).equals(UserRole.USER);
    }

}
