package ru.edu.ProjectShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.ProjectShop.entity.Product;
import ru.edu.ProjectShop.service.ProductService;

import java.util.List;

@Controller
public class WelcomeController {

    private final ProductService productService;

    @Autowired
    public WelcomeController(ProductService productService) {
        this.productService = productService;
    }

    /**
     * Главная страница магазина
     * @param model
     * @return
     */
    @GetMapping("/")
    public String welcome(Model model) {
        final List<Product> products = productService.allProducts();
        model.addAttribute("products", products);
        return "welcome";
    }
}
