package ru.edu.ProjectShop.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.edu.ProjectShop.security.AwesomeShopAccountPrincipal;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * Custom ErrorController
 */
@Controller
public class CustomErrorController implements ErrorController {
    /**
     * Error page
     */
    @RequestMapping("/error")
    public String handleError(Model model, HttpServletRequest request, @AuthenticationPrincipal AwesomeShopAccountPrincipal principal) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        model.addAttribute("current", principal.getAccount());

        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if (statusCode == HttpStatus.FORBIDDEN.value()) {

                return "error/access_denied";
            } else if (statusCode == HttpStatus.BAD_REQUEST.value()) {

                return "error/400_error";
            }
        }

        return "error/error";
    }
}
