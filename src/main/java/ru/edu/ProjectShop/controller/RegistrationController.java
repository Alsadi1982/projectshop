package ru.edu.ProjectShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.service.RegistrationService;
import ru.edu.ProjectShop.validator.EmailValidator;
import ru.edu.ProjectShop.validator.SimplePasswordValidator;

@Controller
public class RegistrationController {

    private boolean passMatch = true;
    private boolean userExist = false;
    private boolean isEmailValid = true;
    private boolean isPasswordValid = true;

    private final RegistrationService registrationService;
    private final EmailValidator emailValidator;
    private final SimplePasswordValidator simplePasswordValidator;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(RegistrationService registrationService,
                                  EmailValidator emailValidator,
                                  SimplePasswordValidator simplePasswordValidator,
                                  PasswordEncoder passwordEncoder) {
        this.registrationService = registrationService;
        this.emailValidator = emailValidator;
        this.simplePasswordValidator = simplePasswordValidator;
        this.passwordEncoder = passwordEncoder;
    }


    /**
     * Get запрос рендерит страницу с формой регистрации пользователя
     *
     * @param model
     * @return
     */
    @GetMapping("/registration")
    public String registrationForm(Model model) {
        model.addAttribute("passMatch", passMatch);
        model.addAttribute("userExist", userExist);
        model.addAttribute("isEmailValid", isEmailValid);
        model.addAttribute("isPasswordValid", isPasswordValid);
        passMatch = true;
        userExist = false;
        return "registration";
    }

    /**
     * Post запрос регистрирует нового пользователя, проверяет правильность ввода пароля, проверяет уникальность email
     * в случае ошибки при вводе пароля или если email совпадает с email уже зарегистрированного пользователя выводит
     * соответствующее предупреждающее сообщение и сбрасывает форму регистрации
     *
     * @param firstname - имя пользователя
     * @param lastname  - фамилия пользователя
     * @param email  - уникальный email
     * @param password1 - придуманный пароль пользователя
     * @param password2 - проверка ввода пароля
     */
    @PostMapping("/registration")
    public String registration(@RequestParam("firstname") String firstname,
                               @RequestParam("lastname") String lastname,
                               @RequestParam("email") String email,
                               @RequestParam("password1") String password1,
                               @RequestParam("password2") String password2) {

        if (!emailValidator.patternMatches(email)) {
            isEmailValid = false;
            return "redirect:/registration";
        } else {
            isEmailValid = true;
        }

        if(!simplePasswordValidator.isValid(password1)) {
            isPasswordValid = false;
            return "redirect:/registration";
        } else {
            isPasswordValid = true;
        }


        if (registrationService.getAccountByEmail(email) != null) {
            userExist = true;
            return "redirect:/registration";
        } else {
            userExist = false;
        }

        if (!password1.equals(password2)) {
            passMatch = false;
            return "redirect:/registration";
        } else {
            passMatch = true;
        }
        Account account = new Account();
        account.setFirstName(firstname);
        account.setLastName(lastname);
        account.setEmail(email);
        account.setPassword(passwordEncoder.encode(password1));

        registrationService.saveAccount(account);
        return "redirect:/";

    }
}
