package ru.edu.ProjectShop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import ru.edu.ProjectShop.security.MySimpleUrlAuthenticationSuccessHandler;


@Configuration
@EnableWebSecurity
public class AwesomeShopSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Метод AwesomeShopSecurityConfig#configure(HttpSecurity http) конфигурирует доступ к страницам приложения в зависимости от роли пользователя
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/registration").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .successHandler(myAuthenticationSuccessHandler());
    }

    /**
     * Определяет кастомный обработчик успешного перехода на ту или иную страницу в зависимости от роли пользователя
     *
     * @return
     */
    @Bean
    public AuthenticationSuccessHandler myAuthenticationSuccessHandler() {
        return new MySimpleUrlAuthenticationSuccessHandler();
    }

    /**
     * Хэширует все пароли пользователей
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }
}
