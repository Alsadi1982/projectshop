package ru.edu.ProjectShop.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.edu.ProjectShop.entity.Account;
import ru.edu.ProjectShop.repository.AccountRepository;

@Service
public class AwesomeShopUserDetailsService implements UserDetailsService {

    private final AccountRepository accountRepository;

    @Autowired
    public AwesomeShopUserDetailsService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final Account accountInfo = accountRepository.findAccountByEmail(username);

        if (accountInfo == null) {
            throw new UsernameNotFoundException("User with login: " + username + " not found!");
        }
        return new AwesomeShopAccountPrincipal(accountInfo);
    }
}
