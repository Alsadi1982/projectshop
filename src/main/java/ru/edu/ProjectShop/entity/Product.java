package ru.edu.ProjectShop.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Product implements Cloneable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "bg_title")
        private String bgTitle;

    @Column(name = "Image_URL")
    private String imgUrl;

    @Column(nullable = false)
    private BigDecimal price;

    private int amount;

    private String description;

    public boolean isSoldOut() {
        return this.amount == 0;
    }

}
